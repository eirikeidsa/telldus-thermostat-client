import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavComponent} from './nav/nav.component';
import {FooterComponent} from './footer/footer.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {CronComponent} from './cron/cron.component';
import {DesiredTempComponent} from './desired-temp/desired-temp.component';
import {CurrentTempComponent} from './current-temp/current-temp.component';
import {HttpModule} from '@angular/http';
import { RoomComponent } from './room/room.component';
import { FormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NewRoomComponent } from './new-room/new-room.component';
import {ApiModule} from '../../projects/telldus-thermostat-client-api/src/lib/api.module';
import {BASE_PATH} from '../../projects/telldus-thermostat-client-api/src/lib/variables';
import {environment} from '../environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FooterComponent,
    DashboardComponent,
    CronComponent,
    DesiredTempComponent,
    CurrentTempComponent,
    RoomComponent,
    NewRoomComponent,
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    HttpModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ApiModule

  ],
  providers: [{provide: BASE_PATH, useValue: environment.url}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
