import {Component, OnInit} from '@angular/core';
import {CronTemperature} from '../../../projects/telldus-thermostat-client-api/src/lib/model/cronTemperature';
import {RoomList} from '../../../projects/telldus-thermostat-client-api/src/lib/model/roomList';
import {Room} from '../../../projects/telldus-thermostat-client-api/src/lib/model/room';
import {RoomService} from '../../../projects/telldus-thermostat-client-api/src/lib/api/room.service';
import {CronService} from '../../../projects/telldus-thermostat-client-api/src/lib/api/cron.service';
import {Cron} from '../../../projects/telldus-thermostat-client-api/src/lib/model/cron';

@Component({
  selector: 'app-cron',
  templateUrl: './cron.component.html',
  styleUrls: ['./cron.component.css'],
  providers: [CronService, RoomService]
})
export class CronComponent implements OnInit {

  crons: Cron = new class implements Cron {
    crons: Array<CronTemperature>;
  };

  newCron: CronTemperature = new class implements CronTemperature {
    active: boolean;
    cron: string;
    id: string;
    room: string;
    temperature: number;
  };

  rooms: RoomList = new class implements RoomList {
    rooms: Array<Room>;
  };

  roomNames: Map<string, string> = new Map();

  constructor(private cronSerivce: CronService, private roomService: RoomService) {
  }

  ngOnInit() {
    this.roomService.getRoomList().subscribe(res => this.getListOfRooms(res));
  }

  setCrons(crons: Cron) {
    this.crons = crons;
  }

  getRoomName(roomId: string) {
    return this.roomNames.get(roomId);
  }

  private getListOfRooms(rooms: RoomList) {
    this.rooms = rooms;
    rooms.rooms.forEach(room => this.addToMap(room));
    this.cronSerivce.getCronJobs().subscribe(res => this.setCrons(res));
  }

  private addToMap(room: Room) {
    this.roomNames.set(room.id, room.name);
  }

  addNew() {
    this.crons.crons.push(this.newCron);
    this.newCron = new class implements CronTemperature {
      active: boolean;
      cron: string;
      id: string;
      room: string;
      temperature: number;
    };
  }

  saveChanges() {
    this.cronSerivce.postCronJobs(this.crons).subscribe();
  }

  removeCron(index) {
    this.crons.crons.splice(index, 1);
  }

}
