import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RoomList} from '../../../projects/telldus-thermostat-client-api/src/lib/model/roomList';
import {RoomService} from '../../../projects/telldus-thermostat-client-api/src/lib/api/room.service';
import {Room} from '../../../projects/telldus-thermostat-client-api/src/lib/model/room';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css'],
  providers: [RoomService]
})
export class RoomComponent implements OnInit {

  constructor(private roomSerivce: RoomService, private router: Router) {
  }

  rooms: RoomList;

  ngOnInit() {
    this.rooms = new class implements RoomList {
      rooms: Array<Room>;
    };
    this.getRooms();
  }

  getRooms() {
    this.roomSerivce.getRoomList().subscribe(
      res => this.setRooms(res));
  }

  setRooms(rooms: RoomList) {
    this.rooms = rooms;
  }

  editRoom(roomId) {
    this.router.navigateByUrl('/edit-room/' + roomId);

  }

}
