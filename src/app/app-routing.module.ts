import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {CronComponent} from './cron/cron.component';
import {NewRoomComponent} from './new-room/new-room.component';

const routes: Routes = [
  {path: 'cron', component: CronComponent},
  {path: 'new-room', component: NewRoomComponent},
  {path: 'edit-room/:id', component: NewRoomComponent},
  {path: '', component: DashboardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
