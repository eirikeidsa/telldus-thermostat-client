import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesiredTempComponent } from './desired-temp.component';

describe('DesiredTempComponent', () => {
  let component: DesiredTempComponent;
  let fixture: ComponentFixture<DesiredTempComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesiredTempComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesiredTempComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
