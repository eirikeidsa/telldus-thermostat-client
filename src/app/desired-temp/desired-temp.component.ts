import {Component, OnInit, Input} from '@angular/core';
import {Room} from '../../../projects/telldus-thermostat-client-api/src/lib/model/room';
import {RoomService} from '../../../projects/telldus-thermostat-client-api/src/lib/api/room.service';
import {TemperatureStatus} from '../../../projects/telldus-thermostat-client-api/src/lib/model/temperatureStatus';

@Component({
  selector: 'app-desired-temp',
  templateUrl: './desired-temp.component.html',
  styleUrls: ['./desired-temp.component.css'],
  providers: [RoomService]
})
export class DesiredTempComponent implements OnInit {

  @Input() room: Room;

  constructor(private roomService: RoomService) {
  }

  ngOnInit() {
  }

  setTemperatureUp() {
    this.roomService.putTemperature(this.room.id, this.room.desiredTemperature + 1).subscribe(
      res => this.setTemperature(res)
    );
  }

  setTemperatureDown() {
    this.roomService.putTemperature(this.room.id, this.room.desiredTemperature - 1).subscribe(
      res => this.setTemperature(res)
    );
  }

  setTemperature(desiredTemp: TemperatureStatus) {
    this.room.desiredTemperature = desiredTemp.desiredTemperature;
    this.room.currentTemperature = desiredTemp.currentTemperature;
  }

}
