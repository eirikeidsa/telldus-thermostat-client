import {Component, Input, OnInit} from '@angular/core';
import {Room} from '../../../projects/telldus-thermostat-client-api/src/lib/model/room';

@Component({
  selector: 'app-current-temp',
  templateUrl: './current-temp.component.html',
  styleUrls: ['./current-temp.component.css']
})
export class CurrentTempComponent implements OnInit {

  @Input() room: Room;

  constructor() {
  }

  ngOnInit() {
  }

}
