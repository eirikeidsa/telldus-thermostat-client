import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RoomService} from '../../../projects/telldus-thermostat-client-api/src/lib/api/room.service';
import {Room} from '../../../projects/telldus-thermostat-client-api/src/lib/model/room';
import {TelldusService} from '../../../projects/telldus-thermostat-client-api/src/lib/api/telldus.service';
import {TelldusList} from '../../../projects/telldus-thermostat-client-api/src/lib/model/telldusList';
import {Telldus} from '../../../projects/telldus-thermostat-client-api/src/lib/model/telldus';

@Component({
  selector: 'app-new-room',
  templateUrl: './new-room.component.html',
  styleUrls: ['./new-room.component.css'],
  providers: [RoomService]
})
export class NewRoomComponent implements OnInit, OnDestroy {

  room: Room = new class implements Room {
    currentTemperature: number;
    desiredTemperature: number;
    deviceId: string;
    flexTemperature: number;
    id: string;
    name: string;
    sensorId: string;
    deviceEffect: number;
  };

  test: string;
  test2: string;

  telldus: TelldusList = new class implements TelldusList {
    device: Array<Telldus>;
    sensor: Array<Telldus>;
  };

  id: string;
  private sub: any;

  constructor(private roomService: RoomService, private telldusService: TelldusService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.getTelldusInfo();

    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.roomService.getRoom(this.id).subscribe(response => this.room = response);
      }
    });

  }

  save() {
    this.roomService.postRoom(this.room).subscribe(response => this.routeToDashboard());
    this.routeToDashboard();
  }

  routeToDashboard() {
    this.router.navigateByUrl('/');
  }

  getTelldusInfo() {
    this.telldusService.getTelldus().subscribe(response => this.telldus = response);
  }

  delete(roomId) {
    this.roomService.deleteRoom(roomId).subscribe();
    this.routeToDashboard();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }


}
