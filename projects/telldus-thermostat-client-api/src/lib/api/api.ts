export * from './cron.service';
import { CronService } from './cron.service';
export * from './database.service';
import { DatabaseService } from './database.service';
export * from './room.service';
import { RoomService } from './room.service';
export * from './telldus.service';
import { TelldusService } from './telldus.service';
export const APIS = [CronService, DatabaseService, RoomService, TelldusService];
