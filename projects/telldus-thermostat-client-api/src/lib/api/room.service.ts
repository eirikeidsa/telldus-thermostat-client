
import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs';

import { Room } from '../model/room';
import { RoomList } from '../model/roomList';
import { TemperatureStatus } from '../model/temperatureStatus';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class RoomService {

    protected basePath = 'https://localhost/api';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (let consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    public deleteRoom(roomId: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public deleteRoom(roomId: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public deleteRoom(roomId: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public deleteRoom(roomId: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (roomId === null || roomId === undefined) {
            throw new Error('Required parameter roomId was null or undefined when calling deleteRoom.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.delete<any>(`${this.basePath}/rooms/${encodeURIComponent(String(roomId))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    public getRoom(roomId: string, observe?: 'body', reportProgress?: boolean): Observable<Room>;
    public getRoom(roomId: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Room>>;
    public getRoom(roomId: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Room>>;
    public getRoom(roomId: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (roomId === null || roomId === undefined) {
            throw new Error('Required parameter roomId was null or undefined when calling getRoom.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.get<Room>(`${this.basePath}/rooms/${encodeURIComponent(String(roomId))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    public getRoomList(observe?: 'body', reportProgress?: boolean): Observable<RoomList>;
    public getRoomList(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<RoomList>>;
    public getRoomList(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<RoomList>>;
    public getRoomList(observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
            'application/json'
        ];

        return this.httpClient.get<RoomList>(`${this.basePath}/rooms`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    public getTemperatureStatus(roomId: string, observe?: 'body', reportProgress?: boolean): Observable<TemperatureStatus>;
    public getTemperatureStatus(roomId: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<TemperatureStatus>>;
    public getTemperatureStatus(roomId: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<TemperatureStatus>>;
    public getTemperatureStatus(roomId: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (roomId === null || roomId === undefined) {
            throw new Error('Required parameter roomId was null or undefined when calling getTemperatureStatus.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.get<TemperatureStatus>(`${this.basePath}/rooms/${encodeURIComponent(String(roomId))}/temperature`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    public postRoom(body: Room, observe?: 'body', reportProgress?: boolean): Observable<Room>;
    public postRoom(body: Room, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Room>>;
    public postRoom(body: Room, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Room>>;
    public postRoom(body: Room, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling postRoom.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
            'application/json'
        ];
        let httpContentTypeSelected:string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set("Content-Type", httpContentTypeSelected);
        }

        return this.httpClient.post<Room>(`${this.basePath}/rooms`,
            body,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    public putRoom(roomId: string, body: Room, observe?: 'body', reportProgress?: boolean): Observable<Room>;
    public putRoom(roomId: string, body: Room, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Room>>;
    public putRoom(roomId: string, body: Room, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Room>>;
    public putRoom(roomId: string, body: Room, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (roomId === null || roomId === undefined) {
            throw new Error('Required parameter roomId was null or undefined when calling putRoom.');
        }
        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling putRoom.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];
        let httpContentTypeSelected:string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set("Content-Type", httpContentTypeSelected);
        }

        return this.httpClient.put<Room>(`${this.basePath}/rooms/${encodeURIComponent(String(roomId))}`,
            body,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    public putTemperature(roomId: string, temperature: number, observe?: 'body', reportProgress?: boolean): Observable<TemperatureStatus>;
    public putTemperature(roomId: string, temperature: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<TemperatureStatus>>;
    public putTemperature(roomId: string, temperature: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<TemperatureStatus>>;
    public putTemperature(roomId: string, temperature: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (roomId === null || roomId === undefined) {
            throw new Error('Required parameter roomId was null or undefined when calling putTemperature.');
        }
        if (temperature === null || temperature === undefined) {
            throw new Error('Required parameter temperature was null or undefined when calling putTemperature.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.put<TemperatureStatus>(`${this.basePath}/rooms/${encodeURIComponent(String(roomId))}/temperature/${encodeURIComponent(String(temperature))}`,
            null,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
